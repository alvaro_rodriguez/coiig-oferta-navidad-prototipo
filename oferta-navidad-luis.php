<!-- Oferta navidad Luis -->
<div class="grid9" id="content">
<h1>
    Oferta navidad
</h1>
<style type="text/css">
    #summary-head {
    height: 30px;
    max-height: 30px;
    padding: 16px 16px 16px 0px;
    background-color: #EEE;
    font-size: 1.5em;
    cursor: pointer;
    text-indent: 5px;
  }

  #summary-body {
    height: 200px;
    max-height: 200px;
    padding: 16px;
    background-color: #EEE;
    overflow-y: scroll;
    overflow-x: hidden;
  }
</style>
<!-- uiView: -->
<ui-view class="ng-scope">
    <!-- uiView: -->
    <ui-view class="ng-scope">
        <purchase class="ng-scope ng-isolate-scope">
            <div>
                <p class="note">
                    En caso de no ser miembro del Colegio Oficial de Ingenieros Industriales de Gipuzkoa,rellena el formulario que se muestra a continuación.
                </p>
            </div>
            <purchase-summary class="ng-isolate-scope" purchase="$ctrl.purchase">
                <div class="ng-binding" id="summary-head" ng-click="$ctrl.toggleSummary()">
                    <strong class="ng-binding">€0.00</strong>
                    (Productos 0)
                </div>
                <!-- ngIf: $ctrl.showSummary -->
            </purchase-summary>
            <!-- uiView: -->
            <ui-view class="ng-scope" purchase="$ctrl.purchase">
                <purchase-list class="ng-scope ng-isolate-scope" purchase="$ctrl.purchase">
                    <table width="100%">
                        <thead>
                            <tr>
                                <th>
                                </th>
                                <th style="font-weight:bold;font-size:1.2em;text-align:left">
                                </th>
                                <th style="font-weight:bold;font-size:1.2em;text-align:right">
                                </th>
                                <th style="font-weight:bold;font-size:1.2em;text-align:right">
                                </th>
                                <th style="max-width:100px;width:100px;font-weight:bold;font-size:1.2em;text-align:right">
                                    Total
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/175" src="./index.php_files/175" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Enate Chardonnay
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            VINO BLANCO - SOMONTANO 2016
                                        </strong>
                                        <br>
                                            Bodega Enate
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €47.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/176" src="./index.php_files/176" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Conde Valdemar
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Crianza 2013
                                        </strong>
                                        <br>
                                            Bodega Valdemar
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €63.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/178" src="./index.php_files/178" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Viña Real
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Crianza 2015
                                        </strong>
                                        <br>
                                            C.V.N.E.
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €63.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/180" src="./index.php_files/180" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Vivanco
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Crianza 2013
                                        </strong>
                                        <br>
                                            Vivanco
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €81.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/181" src="./index.php_files/181" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Señorio de Ondarre
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Reserva 2012
                                        </strong>
                                        <br>
                                            Olarra
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €70.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/182" src="./index.php_files/182" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Marqués de Riscal
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Reserva 2013
                                        </strong>
                                        <br>
                                            Herederos M. Riscal
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €70.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/183" src="./index.php_files/183" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Azpilicueta
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Reserva 2012
                                        </strong>
                                        <br>
                                            Domecq
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €46.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/184" src="./index.php_files/184" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Brut Nature NU
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Cava
                                        </strong>
                                        <br>
                                            Maset del Lleó
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €45.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/185" src="./index.php_files/185" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Pimientos piquillo extra
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                        </strong>
                                        <br>
                                            Bujanda
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €52.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/186" src="./index.php_files/186" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Espárragos extra
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                        </strong>
                                        <br>
                                            Bujanda
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €56.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (12 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/187" src="./index.php_files/187" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Aceite Oliva 1.881
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Virgen Extra
                                        </strong>
                                        <br>
                                            Santa Teresa-Osuna
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €92.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/188" src="./index.php_files/188" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Aceite Oliva Hacienda Ortigosa
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Virgen Extra
                                        </strong>
                                        <br>
                                            Trujal Hacienda Ortigosa
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €67.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/189" src="./index.php_files/189" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Bonito del norte (aceite oliva)
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            150 g./escurrido
                                        </strong>
                                        <br>
                                            Serrats
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €49.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/190" src="./index.php_files/190" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Ventresca de bonito (aceite oliva)
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            81 gr./escurrido
                                        </strong>
                                        <br>
                                            Serrats
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €57.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/191" src="./index.php_files/191" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Paleta ibérica bellota fileteada
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            150 gr./sobre
                                        </strong>
                                        <br>
                                            Selección Gourmet
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €93.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/192" src="./index.php_files/192" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Lomo ibérico bellota fileteado
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            100 gr./sobre
                                        </strong>
                                        <br>
                                            Selección Gourmet
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €51.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/193" src="./index.php_files/193" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Chorizo ibérico bellota
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            700 gr. aprox.
                                        </strong>
                                        <br>
                                            Jamones Benito Pérez
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €8.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/194" src="./index.php_files/194" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Salchichón ibérico bellota
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            700 gr. aprox.
                                        </strong>
                                        <br>
                                            Jamones Benito Pérez
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €8.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/195" src="./index.php_files/195" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Foie de pato
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            130 gr./tarro
                                        </strong>
                                        <br>
                                            Zubia
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €61.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/196" src="./index.php_files/196" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Foie de pato
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            130 gr./latas
                                        </strong>
                                        <br>
                                            Delicass
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €44.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/197" src="./index.php_files/197" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Lote turrones
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            6 unidades
                                        </strong>
                                        <br>
                                            Eceiza
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €35.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr class="ng-scope" ng-repeat="producto in $ctrl.purchase.getProductos()">
                                <td style="text-align:left">
                                    <img ng-src="/images/ofertanavidad/id/198" src="./index.php_files/198" style="vertical-align: top; max-height: 80px">
                                    
                                </td>
                                <td class="ng-binding" style="text-align:left;width:300px;max-width: 300px;">
                                    <!-- ngIf: producto.Nuevo == 1 -->
                                    <strong class="ng-binding" style="font-size: 1.2em">
                                        Queso Idiazabal de Orendain
                                    </strong>
                                    <br>
                                        <strong class="ng-binding" style="font-size: 0.9em">
                                            Más de 500gr.
                                        </strong>
                                        <br>
                                            Zelaieta Baserria
                                            <a href="http://colegiado.coiig.com/oferta/colegiado/#" ng-click="$ctrl.toggle[producto.Id] = !$ctrl.toggle[producto.Id]">
                                                Ver descripción
                                            </a>
                                            <br>
                                                <!-- ngIf: $ctrl.toggle[producto.Id] -->
                                            <br>
                                        <br>
                                    <br>
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €11.00
                                </td>
                                <td style="text-align:right">
                                    <input class="ng-pristine ng-untouched ng-valid ng-not-empty" ng-change="$ctrl.purchase.refreshCestaVisible()" ng-model="producto.cantidad" style="width:25px;" type="text">
                                        <button ng-click="$ctrl.purchase.add(producto)" type="button">
                                            +
                                        </button>
                                        <button ng-click="$ctrl.purchase.substract(producto)" type="button">
                                            -
                                        </button>
                                        <br>
                                            <small class="ng-binding" style="font-size:0.8em;color:grey">
                                                caja (6 botellas)
                                            </small>
                                        <br>
                                    
                                </td>
                                <td class="ng-binding" style="text-align:right">
                                    €0.00
                                </td>
                            </tr>
                            <!-- end ngRepeat: producto in $ctrl.purchase.getProductos() -->
                            <tr>
                                <td colspan="3" style="border-top: thin solid black">
                                </td>
                                <td class="ng-binding" style="border-top: thin solid black;text-align:right">
                                    0
                                </td>
                                <td class="ng-binding" style="border-top: thin solid black;text-align:right">
                                    €0.00
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6" style="text-align: center;font-size: 1.5em;padding-top:20px">
                                    <button disabled="disabled" ng-click="$ctrl.hacerPedido()" ng-disabled="$ctrl.purchase.isEmpty()" type="button">
                                        HACER PEDIDO
                                    </button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </purchase-list>
            </ui-view>
        </purchase>
    </ui-view>
</ui-view>
</div>