// All pages common behaviors:
$(document).ready(function() {
	// ToggleVal: limpia los input y textarea al ganar el foco. Revierte el valor original del campo si no se escribe nada.
	$("form").find("input, textarea").toggleVal();
	// Establece una clase para cuando se gana el foco y otra para cuando el contenido del campo ha sido modificado
	$("form").find(".errorField input, .errorField textarea").toggleVal({
	    focusClass: "errorFieldFocus",
	    changedClass: "errorFieldChanged"
	});
	// Antes de enviar un formulario borra los textos por defecto para que éstos no sean enviados a la BBDD
	$("form :text").toggleVal();
  $("form").submit(function() {
    $(this).find(".toggleval").each(function() {
      if($(this).val() == $(this).data("defText")) {
        $(this).val("");
      }
    });
  });
});