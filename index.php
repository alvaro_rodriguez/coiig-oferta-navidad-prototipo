<?php include 'header.php' ?>
<!-- Oferta navidad -->

<div class="grid9 oferta-navidad" id="content">

  <h1>Oferta de Navidad</h1>
  <p class="oferta-introduccion">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed orci ligula. Maecenas sed odio risus. Donec ullamcorper lacus leo, in ullamcorper turpis tincidunt at. Duis semper, eros ut gravida luctus, magna eros convallis neque, ut molestie ante justo quis augue. Aenean sodales egestas massa nec rutrum. </p>
  <p class="note">Último día para hacer pedido: 24 octubre.</p>
  <div id="cesta" class="cesta">
    <!-- <img src="https://icon-icons.com/icons2/67/PNG/128/shoppingcart_compras_13341.png" class="cesta__icono"> -->
    <span class="cesta__total-productos">Tu cesta está vacía / Llevas <strong>8</strong> productos en tu cesta</span>
    <span class="cesta__total-euros"><strong>Total: €0</strong></span>
    <span class="cesta__ver">Ver cesta</span>
  </div>

  <div class="resumen-cesta">
    <span class="resumen-cesta__titulo">Llevas <strong>8</strong> productos en tu cesta</span>
    <ul class="lista-productos-cesta">
      <?php function productoCesta(){ ?>
        <li class="producto-cesta">      
            <div>
              <div class="producto-cesta__imagen">
                <img src="http://colegiado.coiig.com/images/ofertanavidad/id/175">
              </div>
              <div class="producto-cesta__datos-col-1">
                <span class="producto-cesta__nombre">Enate Chardonnay</span>
                <span class="producto-cesta__tipo">Vino Blanco - Somontano</span>
                <span class="producto-cesta__proveedor">Bodega Enate</span>
              </div>
              <div class="producto-cesta__datos-col-2">
                <div>
                  <input class="producto-cesta__input" type="number" min="1" name="">
                  <button class="btn btn-green producto__btn-anadir">+</button>
                  <button class="btn btn-secundario producto__btn-restar">-</button>
                  <span class="producto-cesta__precio">x €105,85</span>
                  <span class="producto-cesta__unidad">Caja (6 botellas)</span>
                </div>
                <div>
                  <span class="producto-cesta__precio-total-producto">€200,43</span>
                </div>
              </div>
            </div>
        </li>
      <?php } ?>
      <?php for($i=0;$i<3;$i++) { productoCesta(); }  ?>
    </ul>
    <div class="resumen-cesta__total">
      <span>Total:</span> <strong>€300</strong>
    </div>
    <div class="resumen-cesta__acciones">
      <button class="btn btn-green">Comprobar pedido</button>
      <a href="">Continuar comprando</a>
    </div>
  </div>
  
  <div class="categoria-productos">Categoría de los productos</div>
  
  <ul class="lista-productos">

    <?php function producto(){ ?>

      <li class="producto">      
          <div>
            <div class="producto__nuevo">Nuevo</div>
            <div class="producto__imagen">
              <img src="http://colegiado.coiig.com/images/ofertanavidad/id/175">
            </div>
            <div class="producto__datos">
              <span class="producto__nombre">Enate Chardonnay</span>
              <span class="producto__tipo">Vino Blanco - Somontano</span>
              <span class="producto__proveedor">Bodega Enate</span>
              <a class="producto__enlace-descripcion">Ver descripción</a>
              <span class="producto__precio">€105,85</span>
              <span class="producto__unidad">Caja (6 botellas)</span>
              <div>
                <input class="producto__input" type="" name="" placeholder="0">
                <button class="btn btn-green producto__btn-anadir">+</button>
                <button class="btn btn-secundario producto__btn-restar">-</button>
              </div>
            </div>
          </div>
      </li>
      
    <?php } ?>
    
    <?php for($i=0;$i<3;$i++) { producto(); }  ?>

  </ul>

  <div class="categoria-productos">Categoría de los productos</div>

  <ul class="lista-productos">
    <?php for($i=0;$i<7;$i++) { producto(); }  ?>
  </ul>

    <div class="resumen-cesta">
    <span class="resumen-cesta__titulo">Llevas <strong>8</strong> productos en tu cesta</span>
    <div class="resumen-cesta__total">
      <span>Total:</span> <strong>€300</strong>
    </div>
    <div class="resumen-cesta__acciones">
      <button class="btn btn-green">Comprobar pedido</button>
    </div>
  </div>

  <button id="abrirModal">Abrir modal</button>

  <div id="unaModal" class="modalDialog">
    <div>
      <span title="Cerrar" class="cerrar">X</span>
      <h2>Soy una modal</h2>
      <p>Soy una modal</p>
    </div>
  </div>

  <script>
    // Get the modal
      var modal = document.getElementById('unaModal');

      // Get the button that opens the modal
      var btn = document.getElementById("abrirModal");

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("cerrar")[0];

      // When the user clicks the button, open the modal 
      btn.onclick = function() {
          modal.style.display = "block";
      }

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        console.log('!!!!!!');
          modal.style.display = "none";
      }

      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
          if (event.target == modal) {
              modal.style.display = "none";
          }
      }
  </script>

</div>

<!-- Modal cesta -->
<div style="display:none;" id="modal-cesta">
  <h3 class="title">Tu cesta</h3>
  <p>resumen de la cesta aquí.</p>
  <div>
    <button class="btn btn-green">Comprobar pedido</button>
    <button class="btn btn-secundario">Continuar comprando</button>
  </div>
</div>

<!-- Modal descripción -->
<div style="display:none;" id="modal-descripcion">
  <h3 class="title">Enate Chardonnay</h3>
  <p>En la zona nordeste de Huesca se encuentra la Región de Somontano, a los pies de los Pirineos.
  Enate es una bodega que se abastece de 500 ha. de viñedo propio situado en el valle de Enate.
  Es un vino muy apreciado y elaborado con la variedad Chardonnay, que realiza un buen maridaje con pescados,
  mariscos, carnes blancas y pastas.</p>
</div>

<script type="text/javascript">
  jQuery(document).ready(function($){
  $('.lightbox').lightbox();
  jQuery(".cesta").click(function(ev) {
    jQuery.lightbox("#modal-cesta", {
      'autoresize'  : true
    }
        );
    ev.preventDefault();
  });
  jQuery(".producto__enlace-descripcion").click(function(ev) {
    jQuery.lightbox("#modal-descripcion", {
      'autoresize'  : true
    }
        );
    ev.preventDefault();
  });
});
</script>

<script>
  $(".producto__btn-anadir, .producto__btn-restar").click(function() {
    setTimeout(function() {
        $("#cesta").addClass("flash-cesta");
    }, 1);
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.4/jquery.sticky.min.js"></script>
 <script>
    $(window).load(function(){
      $("#cesta").sticky({ topSpacing: 0, zIndex: 9});
    });
  </script>
<?php include 'footer.php' ?>
            