<div class="grid-first" id="footer">
                <p class="title">
                    © Gipuzkoako Industri Ingeniarien Elkargo Ofiziala · Colegio Oficial de Ingenieros Industriales de Gipuzkoa
                </p>
                <p class="address">
                    <a href="http://maps.google.es/maps/place?oe=utf-8&amp;rls=org.mozilla:es-ES:official&amp;client=firefox-a&amp;um=1&amp;ie=UTF-8&amp;q=Zubieta+38,+Donostia&amp;fb=1&amp;gl=es&amp;hnear=San+Sebasti%C3%A1n&amp;cid=4893750220773115035" target="_blank" title="Vea nuestra ubicación en Google Maps">
                        c/ Zubieta 38, bajo. 20007 Donostia/San Sebastián
                        <strong>
                            (mapa)
                        </strong>
                    </a>
                    <span>
                        //
                    </span>
                    <strong>
                        Tel.:
                    </strong>
                    943 42 39 18
                    <span>
                        //
                    </span>
                    <strong>
                        E-mail:
                    </strong>
                    <a href="mailto:ingeniariak@ingeniariak.eus" title="Escríbanos un e-mail">
                        ingeniariak@ingeniariak.eus
                    </a>
                </p>
            </div>
            <!--footer-->
        </div>
        <!--/container-->
        <script type="text/javascript">
            $("#error").ajaxError(function (event, request, settings) {
              $(this).append("<p class='error'>Error de conexión con:  " + settings.url + "</p>");
            });
        </script>
        <div class="jquery-lightbox-overlay" id="1504859461355" style="position: fixed; top: 0px; left: 0px; opacity: 0.6; display: none; z-index: 6999;">
        </div>
        <div class="jquery-lightbox-move" style="position: absolute; z-index: 7000; top: -999px; left: -999px;">
            <div class="jquery-lightbox jquery-lightbox-mode-image">
                <div class="jquery-lightbox-border-top-left">
                </div>
                <div class="jquery-lightbox-border-top-middle">
                </div>
                <div class="jquery-lightbox-border-top-right">
                </div>
                <a class="jquery-lightbox-button-close" href="http://colegiado.coiig.com/oferta/colegiado/#close">
                    <span>
                        Close
                    </span>
                </a>
                <div class="jquery-lightbox-navigator">
                    <a class="jquery-lightbox-button-left" href="http://colegiado.coiig.com/oferta/colegiado/#">
                        <span>
                            Previous
                        </span>
                    </a>
                    <a class="jquery-lightbox-button-right" href="http://colegiado.coiig.com/oferta/colegiado/#">
                        <span>
                            Next
                        </span>
                    </a>
                </div>
                <div class="jquery-lightbox-buttons">
                    <div class="jquery-lightbox-buttons-init">
                    </div>
                    <a class="jquery-lightbox-button-left" href="http://colegiado.coiig.com/oferta/colegiado/#">
                        <span>
                            Previous
                        </span>
                    </a>
                    <a class="jquery-lightbox-button-max" href="http://colegiado.coiig.com/oferta/colegiado/#">
                        <span>
                            Maximize
                        </span>
                    </a>
                    <div class="jquery-lightbox-buttons-custom">
                    </div>
                    <a class="jquery-lightbox-button-right" href="http://colegiado.coiig.com/oferta/colegiado/#">
                        <span>
                            Next
                        </span>
                    </a>
                    <div class="jquery-lightbox-buttons-end">
                    </div>
                </div>
                <div class="jquery-lightbox-background">
                </div>
                <div class="jquery-lightbox-html">
                </div>
                <div class="jquery-lightbox-border-bottom-left">
                </div>
                <div class="jquery-lightbox-border-bottom-middle">
                </div>
                <div class="jquery-lightbox-border-bottom-right">
                </div>
            </div>
        </div>
    
<div id="1504862167353" class="jquery-lightbox-overlay" style="position: fixed; top: 0px; left: 0px; opacity: 0.6; display: none; z-index: 6999;"></div><div class="jquery-lightbox-move" style="position: absolute; z-index: 7000; top: -999px; left: -999px;"><div class="jquery-lightbox jquery-lightbox-mode-image"><div class="jquery-lightbox-border-top-left"></div><div class="jquery-lightbox-border-top-middle"></div><div class="jquery-lightbox-border-top-right"></div><a class="jquery-lightbox-button-close" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#close"><span>Close</span></a><div class="jquery-lightbox-navigator"><a class="jquery-lightbox-button-left" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#"><span>Previous</span></a><a class="jquery-lightbox-button-right" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#"><span>Next</span></a></div><div class="jquery-lightbox-buttons"><div class="jquery-lightbox-buttons-init"></div><a class="jquery-lightbox-button-left" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#"><span>Previous</span></a><a class="jquery-lightbox-button-max" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#"><span>Maximize</span></a><div class="jquery-lightbox-buttons-custom"></div><a class="jquery-lightbox-button-right" href="file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html#"><span>Next</span></a><div class="jquery-lightbox-buttons-end"></div></div><div class="jquery-lightbox-background"></div><div class="jquery-lightbox-html"></div><div class="jquery-lightbox-border-bottom-left"></div><div class="jquery-lightbox-border-bottom-middle"></div><div class="jquery-lightbox-border-bottom-right"></div></div></div></body></html>