<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0155)file:///Users/alvaro/proyectos/COIIG/oferta%20de%20navidad/prototipo/Extranet%20-%20Colegio%20Oficial%20de%20Ingenieros%20Industriales%20de%20Gipuzkoa.html -->
<html class=" webkit chrome mac js webkit chrome mac js" lang="eu" xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}</style>
        
            <style type="text/css">
                @charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}
            </style>
            <script async="" src="./index.php_files/ga.js" type="text/javascript">
            </script>
            <script src="./index.php_files/angular.min.js" type="text/javascript">
            </script>
            <script src="./index.php_files/angular-ui-router.min.js" type="text/javascript">
            </script>
            <script src="./index.php_files/angular-messages.js" type="text/javascript">
            </script>
            <!-- production -->
            <!-- CSS -->
            <link href="./index.php_files/screen.css" media="screen" rel="stylesheet" type="text/css">
                <!-- Datepicker (http://jqueryui.com/demos/datepicker/) -->
                <!--<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>-->
                <!--<link rel="stylesheet" type="text/css" media="screen" href="css/jquery.ui.datepicker.css" />-->
                <link href="./index.php_files/jquery-ui-1.8.12.custom.css" media="screen" rel="stylesheet" type="text/css">
                    <!-- Javascript -->
                    <script src="./index.php_files/jquery.min.js" type="text/javascript">
                    </script>
                    <link href="./index.php_files/jquery.lightbox.css" rel="stylesheet" type="text/css">
                        <!--[if IE 6]>
            <link rel="stylesheet" type="text/css" href="javascript/lightbox/themes/default/jquery.lightbox.ie6.css"/><![endif]-->
                        <script src="./index.php_files/jquery.lightbox.min.js" type="text/javascript">
                        </script>
                        <!-- Limpiar campos de forms -->
                        <script src="./index.php_files/jquery.toggleval_2.1.min.js" type="text/javascript">
                        </script>
                        <script src="./index.php_files/jquery.complexify.js" type="text/javascript">
                        </script>
                        <!-- Eventos comunes a todo el site -->
                        <script src="./index.php_files/common.js" type="text/javascript">
                        </script>
                        <!-- CSS Browser Selector (http://rafael.adm.br/css_browser_selector/) -->
                        <script src="./index.php_files/css_browser_selector.js" type="text/javascript">
                        </script>
                        <script src="./index.php_files/underscore.js" type="text/javascript">
                        </script>
                        <!-- Title -->
                        <title>
                            Extranet - Colegio Oficial de Ingenieros Industriales de Gipuzkoa
                        </title>
                    
                
            
        <link href="css/oferta-productos.css" media="screen" rel="stylesheet" type="text/css">
    </head>
    <body class="body_ongSingle" ng-model-options="{ updateOn: &#39;blur&#39; }">
        <div id="container">
            <div class="grid-first" id="header">
                <p class="logoCoiig">
                    <a href="http://colegiado.coiig.com/">
                        Ingeniariak // Gipuzkoako Industri Ingeniarien Elkargo Ofiziala · Colegio Oficial de Ingenieros Industriales de Gipuzkoa
                    </a>
                </p>
                <div class="userBlock">
                    <p class="username">FRANCISCO JAVIER ZULAICA</p>
                    <ul class="userMenu">
                        <li>
                            <a href="http://colegiado.coiig.com/personal/data">
                                Tus datos
                            </a>
                        </li>
                        <li>
                            <a href="http://colegiado.coiig.com/personal/payments">
                                Tus pagos
                            </a>
                        </li>
                        <!--            <li><a href="/personal/fee">-->
                        <!--</a></li>-->
                        <li>
                            <a href="http://colegiado.coiig.com/personal/visas">
                                Tus visados
                            </a>
                        </li>
                    </ul>
                    <!--userMenu-->
                    <p class="closeSession">
                        <a href="http://colegiado.coiig.com/index/logout">
                            Cerrar sesión
                        </a>
                    </p>
                </div>
                <!--userBlock-->
                <p class="lang">
                    <a href="http://colegiado.coiig.com/index/chglocate">
                        Euskara
                    </a>
                    /
                    <strong>
                        Castellano
                    </strong>
                    <span class="corner">
                    </span>
                </p>
                <!--lang-->
            </div>
            <!--header-->
            <div class="grid-first grid3" id="menu">
                <div class="submenu">
                    <p class="h hCourses">
                        <span>
                            Formación
                        </span>
                    </p>
                    <ul class="options">
                        <li class="section">
                            <ul>
                                <li>
                                    <a href="http://colegiado.coiig.com/courses">Agenda</a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/courses/inscribed">Tus inscripciones</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--submenu-->
                <div class="submenu">
                    <p class="h hServices">
                        <span>
                            Servicios
                        </span>
                    </p>
                    <ul class="options">
                        <li class="section">
                            <p class="title">
                                Sociedad Ernio
                            </p>
                            <ul>
                                <li>
                                    <a href="http://colegiado.coiig.com/ernio/book">
                                        Reservar
                                    </a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/ernio/bookings">
                                        Tus reservas
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="section">
                            <p class="title">
                                Salas de reuniones
                            </p>
                            <ul>
                                <li>
                                    <a href="http://colegiado.coiig.com/room/rent">
                                        Reservar
                                    </a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/room/rentals">
                                        Tus reservas
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="section">
                            <p class="title">
                                Equipos a medida
                            </p>
                            <ul>
                                <li>
                                    <a href="http://colegiado.coiig.com/tools/rents">
                                        Alquilar
                                    </a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/tools/rentals">
                                        Tus alquileres
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <div class="submenu">
                            <p class="h hDocuments">
                                <span>
                                    Documentos
                                </span>
                            </p>
                            <ul class="options">
                                <li class="section">
                                    <ul>
                                        <li>
                                            <a href="http://colegiado.coiig.com/personal/visados">
                                                Visados
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://colegiado.coiig.com/personal/empleo">
                                                Empleo
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--submenu-->
                    </ul>
                </div>
                <div class="submenu">
                    <p class="h hOthers">
                        <span>
                            Otros
                        </span>
                    </p>
                    <ul class="options">
                        <li class="section">
                            <ul>
                                <li>
                                    <a href="http://colegiado.coiig.com/ong">
                                        ONG
                                    </a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/suscriptions/make/ide/6">
                                        Revista DYNA
                                    </a>
                                </li>
                                <li>
                                    <a href="http://colegiado.coiig.com/oferta/index">
                                        Oferta navidad
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--submenu-->
            </div>
            <!--menu-->